<%@ page import="javax.servlet.annotation.WebServlet" %>
<%@ page import="pl.edu.agh.KGosci.Feedback" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.io.IOException" %>
<%@ page import="java.io.PrintWriter" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%-- Created by IntelliJ IDEA. --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title></title>
  </head>
  <body>
  <div id="FeedbackForm">
    <h2>Please submit your feedback: </h2>
  <form action="ProcessFeedback" method="post">
    Your name: <input type="text" name="name"><br>
    Your e-mail address: <input type="text" name="email"><br>
    Comment: <input type="text" name="comment"><br>
    <input type="submit" value="Send Feedback"><br>
  </form>
  <hr>
  </div>
  <div id="FeedbackList">

    <jsp:useBean id="feedback" class="pl.edu.agh.KGosci.ProcessFeedback" scope="application">
    <%
      for (Feedback f: feedback.getFeedbacks()) {
        out.print(feedback.getFeedbacks());
//        out.print("<b>"+f.getName()+" " + f.getEmail() + f.getComment()+ "}</b> (${feedback.email}) says: <br>"+
//            "<span>${feedback.comment}</span><br><br>");
//      }
      }
    %>
    </jsp:useBean>
  <c:forEach var="feedback" items="${feedbacks}">

    <b>${feedback.name}</b> (${feedback.email}) says: <br>
    <span>${feedback.comment}</span><br><br>
  </c:forEach>

  </div>

  </body>
</html>