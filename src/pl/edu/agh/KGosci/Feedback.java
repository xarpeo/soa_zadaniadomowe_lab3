package pl.edu.agh.KGosci;

/**
 * Created by Michał Piotrowski on 2017-03-30.
 */
public class Feedback {

    private String name;
    private String email;
    private String comment;

//    public Feedback(String n, String e, String c) {
//        name = n; email = e; comment = c;
//    }

    public Feedback() {

    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getComment() {
        return comment;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }


}
