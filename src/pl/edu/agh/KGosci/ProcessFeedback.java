package pl.edu.agh.KGosci;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Vector;

/**
 * Created by Michał Piotrowski on 2017-03-30.
 */
@WebServlet("/ProcessFeedback")
public class ProcessFeedback extends HttpServlet {

    public Vector<Feedback> getFeedbacks() {
        return feedbacks;
    }

    public void setFeedbacks(Vector<Feedback> feedbacks) {
        this.feedbacks = feedbacks;
    }

    public Vector<Feedback> feedbacks;

    public ProcessFeedback() {
        feedbacks = new Vector<Feedback>();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        String email = req.getParameter("email");
        String comment = req.getParameter("comment");
        Feedback f = new Feedback();
        f.setComment(comment);
        f.setName(name);
        f.setEmail(email);
        feedbacks.add(f);

        req.setAttribute("feedbacks", feedbacks);
        RequestDispatcher view=req.getRequestDispatcher("index.jsp");
        view.forward(req,resp);


    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher view=req.getRequestDispatcher("index.jsp");
        view.forward(req,resp);
    }
}
